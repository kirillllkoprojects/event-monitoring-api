# Тестовое задание по регистрации событий

Приложение написано с использованием [Spring Boot](http://projects.spring.io/spring-boot/)

## Запуск

Можно запустить локально путем запуска класса ApiApplication. 

## Описание

Проект построен таким образом, что для поиска кол-ва событий нам не придется отталкиваться от времени и искать какие-либо вхождения.
Есть внутренний кэш [Hazelcast](https://hazelcast.org), который аккумулирует в себе 3 мапы minute-events, hour-events, day-events, в 
которые зарегистрированные события по нисходящей опускаются так, как если бы это было при создании нового объекта и прохода GC 
(Eden => Survivor => Tenured).

Мы используем время с расширением до микросекунд(можно и до нано-, но не посчитал нужным) для того, чтоб наши 10000 rpc имели уникальные ключи.
При выходе из мапы с minute events (т.е. когда это событие больше не может считаться прошедшим за последнюю минуту) срабатывает LocalEntryListener,
который перемапливает данное событие в hour events, а затем в daily events. 

LocalEntryListener отвечает только за те entry которые были положены в инстансе именно этого сервиса, таким образом мы избегаем data racing при условии, 
что приложение было запущено с более чем одним интсансом.

## План развертки при условии больших нагрузок
- Обернуть сервис в docker, использовать оркестратор типа Mesos&Marathon || Kubernetes
- Для 10000 rps понадобится выделенный кластер Hazelcast как минимум с тремя нодами, и увеличение backupCount до 3
- Далее для максимально эффективного использования ресурсов можно написать скрипт для marathon(исходники есть в открытом доступе), 
  который будет тушить ноды приложения если нагрузка упадет ниже заданной планки (поднимать новый инстанс в случае когда нагрузка 
  начнет превышать заданную по умолчанию)
- Можно внедрить в данную систему spring web flux, но для успешной работы в таком случае нужно, чтобы и клиент на строне
  регистриующего сервиса был реактивным.

