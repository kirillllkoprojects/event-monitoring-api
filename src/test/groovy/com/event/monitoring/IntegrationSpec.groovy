package com.event.monitoring

import com.event.monitoring.model.Event
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import java.time.LocalDateTime

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@ContextConfiguration(loader = SpringBootContextLoader.class, classes = [ApiApplication.class])
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
class IntegrationSpec extends Specification {

    @Autowired
    WebApplicationContext context

    def mockMvc

    def mapper = new ObjectMapper()

    def setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build()
    }

    def "STATUS 200, calling POST /events and save in minute cache and after one minute confirm, that event was moved to hour cache"() {

        def minuteEvent = new Event()
        minuteEvent.setEventTime(LocalDateTime.now())

        when:
        mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(minuteEvent)))
        def minuteResponse = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/minute"))
        def hourResponse = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/hour"))
        def dayResponse = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/day"))
        sleep(60 * 1000)
        def minuteResponseAfterMinute = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/minute"))
        def hourResponseAfterMinute = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/hour"))
        def dayResponseAfterMinute = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/day"))

        then:
        minuteResponse
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-one.json').text))
        hourResponse
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-one.json').text))
        dayResponse
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-one.json').text))
        minuteResponseAfterMinute
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-zero.json').text))
        hourResponseAfterMinute
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-one.json').text))
        dayResponseAfterMinute
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-one.json').text))
    }

    def "STATUS 200, calling POST /events and save in minute cache if event was hit less then a minute ago"() {

        def minuteEvent = new Event()
        minuteEvent.setEventTime(LocalDateTime.now().minusSeconds(10))

        when:
        def response = mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(minuteEvent)))

        then:
        response
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-minute.json').text))
    }

    def "STATUS 200, calling POST /events and save in hour cache if event was hit less then an hour ago"() {

        def minuteEvent = new Event()
        minuteEvent.setEventTime(LocalDateTime.now().minusMinutes(50))

        when:
        def response = mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(minuteEvent)))

        then:
        response
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-hour.json').text))
    }

    def "STATUS 200, calling POST /events and save in day cache if event was hit less then a day ago"() {

        def minuteEvent = new Event()
        minuteEvent.setEventTime(LocalDateTime.now().minusHours(5))

        when:
        def response = mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(minuteEvent)))

        then:
        response
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-day.json').text))
    }

    def "STATUS 200, calling POST /events and don't save in any cache if event was hit more then a day ago"() {

        def minuteEvent = new Event()
        minuteEvent.setEventTime(LocalDateTime.now().minusDays(2))

        when:
        def response = mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(minuteEvent)))

        then:
        response
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/200/events-old.json').text))
    }

    def "STATUS 400, calling POST /events because of incorrect localDateFormat"() {

        when:
        def response = mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getClass().getResource('request/400/events.json').text))

        then:
        response
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/400/events.json').text))
    }

    def "STATUS 400, calling POST /events because of event will be in future (is it possible?)"() {

        when:
        def response = mockMvc
                .perform(MockMvcRequestBuilders.post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getClass().getResource('request/400/events-future.json').text))

        then:
        response
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().json(this.getClass().getResource('response/400/events-future.json').text))
    }


}
