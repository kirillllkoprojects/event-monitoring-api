import ch.qos.logback.classic.encoder.PatternLayoutEncoder

import static ch.qos.logback.classic.Level.INFO

def appenderList = ["console"]

appender("console", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %X{X-B3-TraceId:-} %X{X-B3-SpanId:-} %highlight(%-5level) [%thread] %cyan(%logger{15}) - %M - %msg%n"
    }
}

root(INFO, appenderList)