package com.event.monitoring.controller;

import com.event.monitoring.exception.ExceptionSerialize;
import com.event.monitoring.exception.TimeBreakingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
@Slf4j
public class EventControllerAdvice {

    @ExceptionHandler({HttpMessageNotReadableException.class, TimeBreakingException.class})
    @ResponseStatus(BAD_REQUEST)
    public ExceptionSerialize validationExceptionHandling(final Exception e) {
        ExceptionSerialize exception = new ExceptionSerialize()
                .setMessage(e.getMessage())
                .setCode(BAD_REQUEST.value());
        log.error(exception.getMessage());
        return exception;
    }

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ExceptionSerialize internalErrorHandling(final Exception e) {
        ExceptionSerialize exception = new ExceptionSerialize()
                .setMessage(String.format("internal server error: %s", e.toString()))
                .setCode(INTERNAL_SERVER_ERROR.value());
        log.error(exception.getMessage());
        return exception;
    }
}
