package com.event.monitoring.controller;

import com.event.monitoring.exception.ExceptionSerialize;
import com.event.monitoring.model.CounterValue;
import com.event.monitoring.model.Event;
import com.event.monitoring.model.HandleEventResponse;
import com.event.monitoring.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.event.monitoring.config.HazelcastProperties.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/events")
@Api(value = "Events", description = "REST API для регистрации событий", tags = {"Events"})
public class EventController {

    private final EventService eventService;

    @ApiOperation(value = "Зарегистрировать событие")
    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 200, response = HandleEventResponse.class, message = "Событие успешно зарегестрировано в системе"),
            @ApiResponse(code = 400, response = ExceptionSerialize.class, message = "Передан некорректный eventTime внутри объекта Event")
    })
    public HandleEventResponse handleEvent(@RequestBody @Valid Event event) {
        return eventService.handleEvent(event);
    }

    @ApiOperation(value = "Получить число событий за последнюю минуту")
    @GetMapping("/minute")
    @ApiResponses({
            @ApiResponse(code = 200, response = CounterValue.class, message = "Получено число зарегистрированных событий за последнюю минуту"),
    })
    public CounterValue getMinuteEventsCounter() {
        return eventService.getCounter(MINUTE_COUNTER);
    }

    @ApiOperation(value = "Получить число событий за последний час")
    @GetMapping("/hour")
    @ApiResponses({
            @ApiResponse(code = 200, response = CounterValue.class, message = "Получено число зарегистрированных событий за последний час"),
    })
    public CounterValue getHourEventsCounter() {
        return eventService.getCounter(HOUR_COUNTER);
    }

    @ApiOperation(value = "Получить число событий за последние сутки")
    @GetMapping("/day")
    @ApiResponses({
            @ApiResponse(code = 200, response = CounterValue.class, message = "Получено число зарегистрированных событий за последние сутки"),
    })
    public CounterValue getDayEventsCounter() {
        return eventService.getCounter(DAY_COUNTER);
    }

}
