package com.event.monitoring.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CounterValue {
    private long events;
}
