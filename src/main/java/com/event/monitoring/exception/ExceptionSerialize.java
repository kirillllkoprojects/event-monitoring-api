package com.event.monitoring.exception;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ExceptionSerialize {
    @ApiModelProperty(value = "Сообщение об ошибке", example = "Некорректный appId")
    private String message;

    @ApiModelProperty(value = "Код ошибки", example = "400")
    private int code;
}