package com.event.monitoring.exception;

import java.time.LocalDateTime;

public class TimeBreakingException extends RuntimeException {
    public TimeBreakingException(LocalDateTime eventTime) {
        super(String.format("time of event: %s is in the future... did you invent a time machine?", eventTime.toString()));
    }
}
