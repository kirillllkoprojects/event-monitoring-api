package com.event.monitoring.config;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.map.listener.EntryEvictedListener;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

import static com.event.monitoring.config.HazelcastProperties.DAY_COUNTER;
import static com.event.monitoring.config.HazelcastProperties.SECONDS_IN_HOUR;

@Service
@RequiredArgsConstructor
public class HourEntryListener implements EntryEvictedListener<String, Long> {

    private final HazelcastInstance hazelcastInstance;

    private IMap<String, Long> dayEventsCounter;

    @PostConstruct
    public void init() {
        dayEventsCounter = hazelcastInstance.getMap(DAY_COUNTER);
    }

    @Override
    public void entryEvicted(EntryEvent<String, Long> event) {
        dayEventsCounter.set(event.getKey(), SECONDS_IN_HOUR - event.getOldValue(), SECONDS_IN_HOUR - event.getOldValue(), TimeUnit.SECONDS);
    }
}
