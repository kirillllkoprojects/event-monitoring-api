package com.event.monitoring.config;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.map.listener.EntryEvictedListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

import static com.event.monitoring.config.HazelcastProperties.HOUR_COUNTER;
import static com.event.monitoring.config.HazelcastProperties.SECONDS_IN_DAY;

@Service
@Slf4j
@RequiredArgsConstructor
public class MinuteEntryListener implements EntryEvictedListener<String, Long> {

    private final HazelcastInstance hazelcastInstance;

    private IMap<String, Long> hourEventsCounter;

    @PostConstruct
    public void init() {
        hourEventsCounter = hazelcastInstance.getMap(HOUR_COUNTER);
    }

    @Override
    public void entryEvicted(EntryEvent<String, Long> event) {
        hourEventsCounter.set(event.getKey(), SECONDS_IN_DAY - event.getOldValue(), SECONDS_IN_DAY - event.getOldValue(), TimeUnit.SECONDS);
    }
}
