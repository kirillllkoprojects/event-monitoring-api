package com.event.monitoring.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "hazelcast")
@ToString(of = {"readBackupData", "backupCount"})
public class HazelcastProperties {

    public static int SECONDS_IN_MINUTE = 60;
    public static int SECONDS_IN_HOUR = 3600;
    public static int SECONDS_IN_DAY = 86400;
    public final static String MINUTE_COUNTER = "minute-events-counter";
    public final static String HOUR_COUNTER = "hour-events-counter";
    public final static String DAY_COUNTER = "day-events-counter";
    public final static String TOO_OLD = "not handled, was more than day ago";

    private boolean readBackupData;
    private int backupCount;
    private int maxCacheSize;
}
