package com.event.monitoring.service;

import com.event.monitoring.config.HourEntryListener;
import com.event.monitoring.config.MinuteEntryListener;
import com.event.monitoring.exception.TimeBreakingException;
import com.event.monitoring.model.CounterValue;
import com.event.monitoring.model.Event;
import com.event.monitoring.model.HandleEventResponse;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import static com.event.monitoring.config.HazelcastProperties.*;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final HazelcastInstance hazelcastInstance;
    private final MinuteEntryListener minuteEntryListener;
    private final HourEntryListener hourEntryListener;

    private IMap<String, Long> minuteEventsCounter;
    private IMap<String, Long> hourEventsCounter;
    private IMap<String, Long> dayEventsCounter;

    @PostConstruct
    public void init() {
        minuteEventsCounter = hazelcastInstance.getMap(MINUTE_COUNTER);
        hourEventsCounter = hazelcastInstance.getMap(HOUR_COUNTER);
        dayEventsCounter = hazelcastInstance.getMap(DAY_COUNTER);
        minuteEventsCounter.addLocalEntryListener(minuteEntryListener);
        hourEventsCounter.addLocalEntryListener(hourEntryListener);
    }


    @Override
    public HandleEventResponse handleEvent(Event event) {
        return HandleEventResponse.builder()
                .result(chooseCounter(event.getEventTime())).build();
    }

    @Override
    public CounterValue getCounter(String counter) {
        int count = 0;
        switch (counter) {
            case MINUTE_COUNTER:
                count = hazelcastInstance.getMap(MINUTE_COUNTER).size();
                break;
            case HOUR_COUNTER:
                count = hazelcastInstance.getMap(MINUTE_COUNTER).size() +
                        hazelcastInstance.getMap(HOUR_COUNTER).size();
                break;
            case DAY_COUNTER:
                count = hazelcastInstance.getMap(MINUTE_COUNTER).size() +
                        hazelcastInstance.getMap(HOUR_COUNTER).size() +
                        hazelcastInstance.getMap(DAY_COUNTER).size();
                break;
        }
        return CounterValue.builder()
                .events(count)
                .build();
    }

    private String chooseCounter(LocalDateTime eventTime) {
        String eventTimeKey = eventTime.toString();
        long deltaTime = ChronoUnit.SECONDS.between(eventTime, LocalDateTime.now());
        if (deltaTime < 0) {
            throw new TimeBreakingException(eventTime);
        }
        long ttlForMinute = SECONDS_IN_MINUTE - deltaTime;
        long ttlForHour = SECONDS_IN_HOUR - deltaTime;
        long ttlForDay = SECONDS_IN_DAY - deltaTime;
        if (deltaTime < SECONDS_IN_MINUTE) {
            minuteEventsCounter.set(eventTimeKey, ttlForMinute, ttlForMinute, TimeUnit.SECONDS);
            return MINUTE_COUNTER;
        } else if (deltaTime < SECONDS_IN_HOUR) {
            hourEventsCounter.set(eventTimeKey, ttlForHour, ttlForHour, TimeUnit.SECONDS);
            return HOUR_COUNTER;
        } else if (deltaTime < SECONDS_IN_DAY) {
            dayEventsCounter.set(eventTimeKey, ttlForDay, ttlForDay, TimeUnit.SECONDS);
            return DAY_COUNTER;
        }
        return TOO_OLD;
    }
}
