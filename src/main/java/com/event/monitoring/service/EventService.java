package com.event.monitoring.service;

import com.event.monitoring.model.CounterValue;
import com.event.monitoring.model.Event;
import com.event.monitoring.model.HandleEventResponse;

public interface EventService {

    HandleEventResponse handleEvent(Event event);

    CounterValue getCounter(String minuteCounter);
}
